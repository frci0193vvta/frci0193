package AngajatiApp.repository;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.List;

import AngajatiApp.model.AgeCriteria;
import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import AngajatiApp.validator.EmployeeException;
import AngajatiApp.validator.EmployeeValidator;
import AngajatiApp.model.SalaryCriteria;

public class EmployeeImpl implements EmployeeRepositoryInterface {

	private static final String ERROR_WHILE_READING_MSG = "Error while reading: ";
	private final String employeeDBFile = "employeeDB/employees.txt";
	private EmployeeValidator employeeValidator = new EmployeeValidator();
	private List<Employee> employeeList = new ArrayList<>();
	private Employee foundEmployee;

	public EmployeeImpl() {
		employeeList = loadEmployeesFromFile();
	}

	@Override
	public boolean addEmployee(Employee employee) {
		employee.setId(employeeList.size());
		if (employeeValidator.isValid(employee)) {
			BufferedWriter bw = null;
			try {
				bw = new BufferedWriter(new FileWriter(employeeDBFile, true));
				bw.write(employee.toString());
				bw.newLine();
				bw.close();
				employeeList.add(employee);

				return true;
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		return false;
	}

	@Override
	public void modifyEmployeeFunction(Employee oldEmployee, DidacticFunction newFunction) {
		oldEmployee.setFunction(newFunction);
	}

	@Override
	public void modifyAddressOrStudiesFunction(Employee foundEmployee, String newAddres, String newStudies) {
		if(!(newAddres.equals(""))) {
			foundEmployee.setAddress(newAddres);
		}

		if(!(newStudies.equals(""))) {
			foundEmployee.setStudies(newStudies);
		}
	}

	@Override
	public void findEmployeeByNameAndSurname(String searchName, String searchSurname) {
		List<Employee> employeeList = loadEmployeesFromFile();
		for (Employee employee:employeeList) {
			if(employee.getFirstName().equals(searchSurname) || employee.getLastName().equals(searchName)){
				System.out.println(employee);
			}
		}
	}

	@Override
	public void deleteEmployee(int idEmployeeDelete){
		List<Employee> employeeListFound = loadEmployeesFromFile();
		for (Employee employee: employeeListFound){
			if(employee.getId() == idEmployeeDelete){
				employeeListFound.remove(employee.getId());
			}
		}
	}

	@Override
	public void modifyAddressOrStudies(int id, String studies, String address) {
		int i = 0;
		while (i < employeeList.size()){
			if(employeeList.get(i).getId() == id){
				employeeList.get(i).setStudies(studies);
				employeeList.get(i).setAddress(address);
			}
		}
	}
	
	private List<Employee> loadEmployeesFromFile() {
		final List<Employee> employeeListLoad = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader(employeeDBFile));){
			String line;
			int counter = 0;
			while ((line = br.readLine()) != null) {
				counter = getCounter(employeeListLoad, line, counter);
			}
		} catch (IOException e) {
			System.err.println(ERROR_WHILE_READING_MSG + e);
		} 
		return employeeListLoad;
	}

	private int getCounter(List<Employee> employeeListLoad, String line, int counter) {
		try {
			final Employee employee = Employee.getEmployeeFromString(line, counter);
			employeeListLoad.add(employee);
			counter++;
		} catch (EmployeeException ex) {
			System.err.println(ERROR_WHILE_READING_MSG + ex.toString());
		}
		return counter;
	}

	@Override
	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	@Override
	public List<Employee> getEmployeeByCriteria() {
		List<Employee> employeeSortedList = new ArrayList<Employee>(employeeList);
		Collections.copy(employeeSortedList, employeeList);
		Collections.sort(employeeSortedList, new AgeCriteria());
		System.out.println(employeeSortedList);
		Collections.sort(employeeSortedList, new SalaryCriteria());
		System.out.println(employeeSortedList);
		return employeeSortedList;
	}

	@Override
	public Employee findEmployeeById(int idOldEmployee) {
		for (Employee employee : employeeList) {
			if (employee.getId() == idOldEmployee) {
				return employee;
			}
		}
		return null;
	}

}
