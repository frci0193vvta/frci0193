package AngajatiApp.repository;

import java.util.List;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;

public interface EmployeeRepositoryInterface {
	
	boolean addEmployee(Employee employee);
	void modifyEmployeeFunction(Employee employee, DidacticFunction newFunction);
	List<Employee> getEmployeeList();
	List<Employee> getEmployeeByCriteria();
	Employee findEmployeeById(int idOldEmployee);
	void modifyAddressOrStudies(int employeeId, String studies, String address);

    void modifyAddressOrStudiesFunction(Employee foundEmployee, String newAddres, String newStudies);

	void findEmployeeByNameAndSurname(String searchName, String searchSurname);

	void deleteEmployee(int idEmployeeDelete);
}
