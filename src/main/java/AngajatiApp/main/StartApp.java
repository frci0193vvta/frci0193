package AngajatiApp.main;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import AngajatiApp.repository.EmployeeImpl;
//import repository.EmployeeMock;
import AngajatiApp.repository.EmployeeRepositoryInterface;

import java.util.Scanner;

import AngajatiApp.controller.EmployeeController;

//functionalitati
//i.	 adaugarea unui nou angajat (nume, prenume, CNP, functia didactica, salariul de incadrare);
//ii.	 modificarea functiei didactice (asistent/lector/conferentiar/profesor) a unui angajat;
//iii.	 afisarea salariatilor ordonati descrescator dupa salariu si crescator dupa varsta (CNP).
public class StartApp {
	
	private static Scanner scanner;
	
	public static void main(String[] args) {
		EmployeeRepositoryInterface employeesRepository = new EmployeeImpl();
		EmployeeController employeeController = new EmployeeController(employeesRepository);
		scanner = new Scanner(System.in);
		while (true) {
			employeeController.printMenu();
			int command;
			try {
				command = scanner.nextInt();
			} catch(Exception e) {
				System.out.println("Exit!");
				return;
			}
			switch (command) {
			case 1:
				Employee employee = getEmployeeFromInput();
				employeeController.addEmployee(employee);
				System.out.println("Employee was added");
				break;
			case 2:
				System.out.println("Dati id-ul angajatului: ");
				int idOldEmployee = scanner.nextInt();
				Employee oldEmployee = employeeController.findEmployeeById(idOldEmployee);
				System.out.println("Dati noua functie didactica: ");
				String newFunction = scanner.next();
				employeeController.modifyEmployee(oldEmployee, getDidacticFunction(newFunction));
				break;
			case 3:
				for(Employee employeeItem : employeeController.getSortedEmployeeList())
				{
					System.out.println(employeeItem.toString());
				}
				break;
			case 4:
					//TODO Modifica adresa/studii angajati
				System.out.println("Dati id-ul angajatului: ");
				int id = scanner.nextInt();
				Employee foundEmployee = employeeController.findEmployeeById(id);
				System.out.println("Dati noua adresa a angajatului: ");
				String newAddres = scanner.next();
				System.out.println("Schimba studiile (pentru a nu modifica apasati doar Enter fara a edita): ");
				String newStudies = scanner.next();
				employeeController.modifyEmployeeAddressStudies(foundEmployee, newAddres, newStudies);
				break;
			case 5:
					//TODO Cauta angajat dupa nume/prenume
				System.out.println("Nume: ");
				String searchName = scanner.next();
				System.out.println("Prenume: ");
				String searchSurname = scanner.next();
					employeeController.findEmployeeByNameAndSurname(searchName, searchSurname);
				break;
			case 6:
					//TODO Program angajati

			case 7:
					//TODO Sterge angajat
				System.out.println("Dati id-ul angajatului: ");
				int idEmployeeDelete = scanner.nextInt();
				employeeController.deleteEmployee(idEmployeeDelete);
				break;
			default:
				System.out.println("Exit!");
				return;
			}
		}
	}

	private static Employee getEmployeeFromInput() {
		System.out.println("First name: ");
		String firstName = scanner.next();
		System.out.println("Last name: ");
		String lastName = scanner.next();
		System.out.println("CNP: ");
		String cnp = scanner.next();
		System.out.println("Functie didactica: ");
		String didacticFuntion = scanner.next();
		System.out.println("Salary: ");
		Double salary = scanner.nextDouble();
		System.out.println("Studies: ");
		String studies = scanner.next();
		System.out.println("Address: ");
		String address = scanner.next();
		return new Employee(firstName, lastName, cnp, getDidacticFunction(didacticFuntion), salary,studies,address);
	}
	
	private static DidacticFunction getDidacticFunction(String didacticFunction) {
		if (didacticFunction.toUpperCase().equals("ASISTENT"))
		{
			return DidacticFunction.ASISTENT;
		}
		if (didacticFunction.toUpperCase().equals("LECTURER"))
		{
			return DidacticFunction.LECTURER;
		}
		if (didacticFunction.toUpperCase().equals("TEACHER"))
		{
			return DidacticFunction.TEACHER;
		}
		if (didacticFunction.toUpperCase().equals("CONFERENTIAR"))
		{
			return DidacticFunction.CONFERENTIAR;
		}
		return DidacticFunction.ASISTENT;
	}

}
