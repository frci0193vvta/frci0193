package AngajatiApp.view;

public class EmployeeView {
	
	public void printMenu() {
		System.out.println("--- Menu ---");
		System.out.println("\t1. Adauga angajat nou");
		System.out.println("\t2. Modifica functia didactica a unui angajat");
		System.out.println("\t3. Afiseaza salariati");
		System.out.println("\t4. Modifica adresa/studii angajat");
		System.out.println("\t5. Cauta angajat dupa nume sau prenume");
		System.out.println("\t6. Program angajat");
		System.out.println("\t7. Sterge angajati");
		System.out.println("\t0. Exit");
	}

}
