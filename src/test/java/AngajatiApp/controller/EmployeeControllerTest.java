package AngajatiApp.controller;

import AngajatiApp.model.Employee;
import AngajatiApp.repository.EmployeeImpl;
import AngajatiApp.repository.EmployeeMock;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeControllerTest {
    EmployeeMock employeeMock;


    @BeforeEach
    void setUp() {
         employeeMock = new EmployeeMock();


    }


    @Test
    void addEmployeeTC1_BB() {


        Employee e1 = new Employee();
        e1.setFirstName("Marius");
        e1.setLastName("Onaca");
        e1.setCnp("2345432345432");
        e1.setFunction(DidacticFunction.ASISTENT);
        e1.setSalary(3100.0);
        e1.setStudies("Universitare");
        e1.setAddress("str.aviatorilor nr.4, Cluj");


        assertTrue(employeeMock.addEmployee(e1));
        System.out.println("Employee successfully added");


    }


    @Test
    void addEmployeeTC2_BB() {


        Employee e1 = new Employee();
        e1.setFirstName("Marius");
        e1.setLastName("Onaca");
        e1.setCnp("32345432345432");// Cnp.length() > 13
        e1.setFunction(DidacticFunction.ASISTENT);
        e1.setSalary(3100.0);
        e1.setStudies("Universitare");
        e1.setAddress("str.aviatorilor nr.4, Cluj");


        assertFalse(employeeMock.addEmployee(e1));
        System.out.println("Employee was not added");
    }

    @Test
    void addEmployeeTC3_BB() {


        Employee e1 = new Employee();
        e1.setFirstName("Marius");
        e1.setLastName("Onaca");
        e1.setCnp("2345432342"); //cnp.length() < 13
        e1.setFunction(DidacticFunction.ASISTENT);
        e1.setSalary(3100.0);
        e1.setStudies("Universitare");
        e1.setAddress("str.aviatorilor nr.4, Cluj");


        assertFalse(employeeMock.addEmployee(e1));
        System.out.println("Employee was not added");

    }

    @Test
    void addEmployeeTC4_BB() {


        Employee e1 = new Employee();
        e1.setFirstName("Marius");
        e1.setLastName("Onaca");
        e1.setCnp("5432345%^&32"); //cnp 5432345%^&32
        e1.setFunction(DidacticFunction.ASISTENT);
        e1.setSalary(3100.0);
        e1.setStudies("Universitare");
        e1.setAddress("str.aviatorilor nr.4, Cluj");

        assertFalse(employeeMock.addEmployee(e1));
        System.out.println("Employee was not added");

    }

    @Test
    void addEmployeeTC5_BB() {

        Employee e1 = new Employee();
        e1.setFirstName("Marius");
        e1.setLastName("Onaca");
        e1.setCnp("2345432345432");
        e1.setFunction(DidacticFunction.ASISTENT);
        e1.setSalary(-1D); //-1
        e1.setStudies("Universitare");
        e1.setAddress("str.aviatorilor nr.4, Cluj");

        assertFalse(employeeMock.addEmployee(e1));
        System.out.println("Employee was not added");
    }


    @Test
    void addEmployeeTC7_BB() {

        Employee e1 = new Employee();
        e1.setFirstName("$%#$"); //$%#$
        e1.setLastName("Onaca");
        e1.setCnp("2345432345432");
        e1.setFunction(DidacticFunction.ASISTENT);
        e1.setSalary(3100.0);
        e1.setStudies("Universitare");
        e1.setAddress("str.aviatorilor nr.4, Cluj");

        assertFalse(employeeMock.addEmployee(e1));
        System.out.println("Employee was not added");

    }

    @Test
    void addEmployeeTC8_BB() {

        Employee e1 = new Employee();
        e1.setFirstName("M");   //M
        e1.setLastName("Onaca");
        e1.setCnp("2345432345432");
        e1.setFunction(DidacticFunction.ASISTENT);
        e1.setSalary(3100.0);
        e1.setStudies("Universitare");
        e1.setAddress("str.aviatorilor nr.4, Cluj");

        assertFalse(employeeMock.addEmployee(e1));
        System.out.println("Employee was not added");

    }

    @Test
    void addEmployeeTC9_BB() {


        Employee e1 = new Employee();
        e1.setFirstName("");    //""
        e1.setLastName("Onaca");
        e1.setCnp("2345432345432");
        e1.setFunction(DidacticFunction.ASISTENT);
        e1.setSalary(3100.0);
        e1.setStudies("Universitare");
        e1.setAddress("str.aviatorilor nr.4, Cluj");

        assertFalse(employeeMock.addEmployee(e1));
        System.out.println("Employee was not added");

    }

    @Test
    void addEmployeeTC10_BB() {

        //firstname.lenght() == 256

        Employee e1 = new Employee();
        e1.setFirstName("Mariusssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss");    //""
        e1.setLastName("Onaca");
        e1.setCnp("2345432345432");
        e1.setFunction(DidacticFunction.ASISTENT);
        e1.setSalary(3100.0);
        e1.setStudies("Universitare");
        e1.setAddress("str.aviatorilor nr.4, Cluj");

        assertFalse(employeeMock.addEmployee(e1));
        System.out.println("Employee was not added");

    }

    @Test
    void addEmployeeTC11_BB() {

        Employee e1 = new Employee();
        e1.setFirstName("Ma");    //"Ma"
        e1.setLastName("Onaca");
        e1.setCnp("2345432345432");
        e1.setFunction(DidacticFunction.ASISTENT);
        e1.setSalary(3100.0);
        e1.setStudies("Universitare");
        e1.setAddress("str.aviatorilor nr.4, Cluj");

        assertFalse(employeeMock.addEmployee(e1));
        System.out.println("Employee was not added");
    }

    @Test
    void addEmployeeTC12_BB() {

        Employee e1 = new Employee();
        e1.setFirstName("Mari");    //"Mari"
        e1.setLastName("Onaca");
        e1.setCnp("2345432345432");
        e1.setFunction(DidacticFunction.ASISTENT);
        e1.setSalary(3100.0);
        e1.setStudies("Universitare");
        e1.setAddress("str.aviatorilor nr.4, Cluj");

        assertTrue(employeeMock.addEmployee(e1));
        System.out.println("Employee successfully added");
    }

    @Test
    void addEmployeeTC13_BB() {


        Employee e1 = new Employee();
        e1.setFirstName("Mariussssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss");    //""
        e1.setLastName("Onaca");
        e1.setCnp("2345432345432");
        e1.setFunction(DidacticFunction.ASISTENT);
        e1.setSalary(3100.0);
        e1.setStudies("Universitare");
        e1.setAddress("str.aviatorilor nr.4, Cluj");
        e1.setId(200);


        assertTrue(employeeMock.addEmployee(e1));
        System.out.println("Employee successfully added");
    }
}