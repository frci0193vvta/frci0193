package AngajatiApp.model;

import AngajatiApp.controller.DidacticFunction;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class EmployeeTest {
    Employee e1,e2,e3;


    @BeforeEach
    public  void SetUP(){
        e1 = new Employee("Razvan","Fratean","0234325534",DidacticFunction.LECTURER, 2000.0, "", "");
        e1.setId(4);
        e2 = new Employee();
        e3 = null;
        System.out.println("Start test");
    }

    @Test
    void constructorEmployee(){
        Employee e4 = new Employee();
        e4.setSalary(2000.0);
        assertNotEquals(null, e4);
        assertEquals(e4.getSalary(), e1.getSalary());
    }

    @Test
    void getId_ofTheEmployee() {
        assertEquals(4, e1.getId());
        assertNotEquals(2, e1.getId());
    }

    @Test
    void setId() {
        assertEquals(0,e2.getId());
        e2.setId(2);
        assertEquals(2,e2.getId());
        assertNotEquals(3,e2.getId());
    }

    @ParameterizedTest
    @ValueSource(ints = {2})
    @Order(2)
    void setId_TestIfParameterIsPassed(int id){
        assertEquals(2, id);
    }


    @Test
    void getFirstName_test1() {
        assertEquals("", e2.getFirstName());
    }

    @Test
    void getFirstName_test2() {
        assertEquals("Razvan", e1.getFirstName());
    }

    @Disabled
    @Test
    void getFirstName_test3() {
        assertEquals("Razvan", e1.getFirstName());
    }

    @Test
    void getFirstName_testNull() {
        try {
            assertEquals("Razvan", e3.getFirstName());
            assert(false);
        }catch (Exception e){
            System.out.println("Exceptia primita este: " + e.toString());
            assert(true);
        }
    }

    @Test
    @Order(1)
    void test_timeout() {
        assertTimeout(Duration.ofSeconds(2), () -> delaySecond(1)); // pass
       // assertTimeout(Duration.ofSeconds(2), () -> delaySecond(4)); // fail
    }

    @ParameterizedTest
    @ValueSource(strings = "")
    void setFirstName_testForEmptyParameter(String firstName) {
        assertTrue(firstName.isEmpty());
    }


    @ParameterizedTest
    @ValueSource(strings = "Razvan")
    void setLastName_testFor( String lastName) {
        assertEquals("Razvan", lastName);
    }

    @Test
    @Order(3)
    void testExpectedException() {

        Assertions.assertThrows(NumberFormatException.class, () -> {
            Integer.parseInt("trei");
        });

    }

    @AfterEach
    public void TearDown(){
        e1 = null;
        e2 = null;
        e3 = null;
        System.out.println("End test");
    }

    void delaySecond(int second) {
        try {
            TimeUnit.SECONDS.sleep(second);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}