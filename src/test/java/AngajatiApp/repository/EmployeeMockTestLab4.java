package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeMockTestLab4 {

    EmployeeMock employeeMock;

    @BeforeEach
    void setUp() {
        employeeMock = new EmployeeMock();
    }

    @Test
    void modifyEmployeeFunction_ifEmployeeDoesNotExistInList() {
        Employee newEmployee = new Employee("Razvan", "Fratean", "5434567890876", DidacticFunction.ASISTENT, 2500d,"postuniv","Cluj");
        newEmployee.setId(9999);
        employeeMock.modifyEmployeeFunction(newEmployee, DidacticFunction.LECTURER);

        assertEquals(null,employeeMock.findEmployeeById(newEmployee.getId()));
    }

    @Test
    void modifyEmployeeFunction_ifEmployeeExist() {
        Employee ionel = new Employee("Ionel", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2500d,"postuniv","Cluj");
        employeeMock.modifyEmployeeFunction(ionel,DidacticFunction.LECTURER);

        assertEquals(DidacticFunction.LECTURER, employeeMock.getEmployeeList().get(ionel.getId()).getFunction());
    }

    @Test
    void  modifyEmployeeFunction_employeenull(){
        List<Employee> employeeList = employeeMock.getEmployeeList();

        Employee ionel = null;
        employeeMock.modifyEmployeeFunction(ionel,DidacticFunction.LECTURER);


        assertEquals(employeeList, employeeMock.getEmployeeList());
    }
}